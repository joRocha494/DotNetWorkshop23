using DotNetWorkshop.Models.Applicants;
using DotNetWorkshop.Services;
using Microsoft.AspNetCore.Mvc;

namespace DotNetWorkshop.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ApplicantController : ControllerBase
    {
        private readonly IApplicantsService _service;

        public ApplicantController(IApplicantsService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<List<ApplicantDTO>> GetApplicants(CancellationToken cancellationToken)
        {
            return await _service.GetApplicants(cancellationToken);
        }

        [HttpGet("{id}")]
        public async Task<ApplicantDTO?> GetApplicant(string id, CancellationToken cancellationToken)
        {
            return await _service.GetApplicant(id, cancellationToken);
        }

        [HttpPost]
        public async Task PostApplicant(CreateUpdateApplicantDTO input, CancellationToken cancellationToken)
        {
            await _service.CreateApplicant(input, cancellationToken);
        }

        [HttpPut("{id}")]
        public async Task PutApplicant(string id, CreateUpdateApplicantDTO input, CancellationToken cancellationToken)
        {
            await _service.UpdateApplicant(id, input, cancellationToken);
        }


        [HttpDelete("{id}")]
        public async Task DeleteApplicant(string id, CancellationToken cancellationToken)
        {
            await _service.DeleteApplicant(id, cancellationToken);
        }
    }
}