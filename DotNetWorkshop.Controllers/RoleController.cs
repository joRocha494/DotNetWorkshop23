﻿using DotNetWorkshop.Models.Roles;
using DotNetWorkshop.Services;
using Microsoft.AspNetCore.Mvc;

namespace DotNetWorkshop.Controllers;

[ApiController]
[Route("[controller]")]
public class RoleController : ControllerBase
{
    private readonly IRolesService _service;

    public RoleController(IRolesService service)
    {
        _service = service;
    }

    [HttpGet]
    public async Task<List<RoleDTO>> GetRoles(CancellationToken cancellationToken)
    {
        return await _service.GetRoles(cancellationToken);
    }

    [HttpGet("{id}")]
    public async Task<RoleDTO?> GetRole(string id, CancellationToken cancellationToken)
    {
        return await _service.GetRole(id, cancellationToken);
    }

    [HttpPost]
    public async Task PostRole(CreateUpdateRoleDTO input, CancellationToken cancellationToken)
    {
        await _service.CreateRole(input, cancellationToken);
    }

    [HttpPut("{id}")]
    public async Task PutRole(string id, CreateUpdateRoleDTO input, CancellationToken cancellationToken)
    {
        await _service.UpdateRole(id, input, cancellationToken);
    }


    [HttpDelete("{id}")]
    public async Task DeleteRole(string id, CancellationToken cancellationToken)
    {
        await _service.DeleteRole(id, cancellationToken);
    }
}
