﻿namespace DotNetWorkshop.Models.Roles;

public class CreateUpdateRoleDTO
{
    public string Name { get; set; } = default!;
}
