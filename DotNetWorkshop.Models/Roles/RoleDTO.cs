﻿namespace DotNetWorkshop.Models.Roles;

public class RoleDTO
{
    public string Id { get; set; } = default!;
    public string Name { get; set; } = default!;
}
