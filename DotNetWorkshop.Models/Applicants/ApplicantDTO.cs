﻿using DotNetWorkshop.Models.Roles;

namespace DotNetWorkshop.Models.Applicants;

public class ApplicantDTO
{
    public string Id { get; set; } = default!;
    public string Name { get; set; } = default!;
    public string? PhoneNumber { get; set; }
    public string Email { get; set; } = default!;
    public List<RoleDTO> Roles { get; set; } = default!;
}
