﻿namespace DotNetWorkshop.Models.Applicants;

public class CreateUpdateApplicantDTO
{
    public string Name { get; set; } = default!;
    public string? PhoneNumber { get; set; }
    public string Email { get; set; } = default!;
    public List<string> RoleIds { get; set; } = default!;
}
