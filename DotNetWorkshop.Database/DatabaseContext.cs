﻿using DotNetWorkshop.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace DotNetWorkshop.Database;

public class DatabaseContext : DbContext
{
    public DbSet<Applicant> Applicants { get; set; } = default!;
    public DbSet<Role> Roles { get; set; } = default!;
    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ApplyConfigurationsFromAssembly(typeof(DatabaseContext).Assembly);
    }
}