﻿using DotNetWorkshop.Models.Roles;
using NCuid;

namespace DotNetWorkshop.Database.Entities;

public class Role
{
    public string Id { get; set; } = Cuid.Generate();
    public string Name { get; set; } = default!;
    public ICollection<Applicant> Applicants { get; set; } = default!;
}

public static class RoleExtensions
{
    public static RoleDTO ToRoleDTO(this Role input)
    {
        return new RoleDTO
        {
            Id = input.Id,
            Name = input.Name
        };
    }

    public static Role ToRoleEntity(this CreateUpdateRoleDTO input)
    {
        return new Role
        {
            Name = input.Name
        };
    }
}