﻿using DotNetWorkshop.Models.Applicants;
using DotNetWorkshop.Models.Roles;
using NCuid;

namespace DotNetWorkshop.Database.Entities;

public class Applicant
{
    public string Id { get; set; } = Cuid.Generate();
    public string Name { get; set; } = default!;
    public string? PhoneNumber { get; set; }
    public string Email { get; set; } = default!;
    public List<Role> Roles { get; set; } = default!;
}

public static class ApplicantExtensions
{
    public static ApplicantDTO ToApplicantDTO(this Applicant input, List<RoleDTO> roleDtos)
    {
        return new ApplicantDTO
        {
            Id = input.Id,
            Name = input.Name,
            PhoneNumber = input.PhoneNumber,
            Email = input.Email,
            Roles = roleDtos
        };
    }

    public static Applicant ToApplicantEntity(this CreateUpdateApplicantDTO input, List<Role> roleEntities)
    {
        return new Applicant
        {
            Name = input.Name,
            PhoneNumber = input.PhoneNumber,
            Email = input.Email,
            Roles = roleEntities
        };
    }
}
