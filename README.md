# AIHealthcare API

## Setup

Restorar as dependências do projeto: dotnet restore

Para criar uma migração:
	- Ir para a pasta do projeto Database
	- dotnet ef migrations add InitialCreate --project ../DotNetWorkshop.Database --startup-project ../DotNetWorkshop.API

Para aplicar as migrações existentes:
	- Ir para a pasta do projeto Database
	- dotnet ef database update --project ../DotNetWorkshop.Database --startup-project ../DotNetWorkshop.API
