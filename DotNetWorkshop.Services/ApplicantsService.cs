﻿using DotNetWorkshop.Database;
using DotNetWorkshop.Database.Entities;
using DotNetWorkshop.Models.Applicants;
using DotNetWorkshop.Models.Roles;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography.X509Certificates;

namespace DotNetWorkshop.Services;

public interface IApplicantsService
{
    Task<List<ApplicantDTO>> GetApplicants(CancellationToken cancellationToken);
    Task<ApplicantDTO> GetApplicant(string id, CancellationToken cancellationToken);
    Task CreateApplicant(CreateUpdateApplicantDTO input, CancellationToken cancellationToken);
    Task UpdateApplicant(string id, CreateUpdateApplicantDTO input, CancellationToken cancellationToken);
    Task DeleteApplicant(string id, CancellationToken cancellationToken);
}

public class ApplicantsService : IApplicantsService
{
    private readonly DatabaseContext _dbContext;

    public ApplicantsService(DatabaseContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<List<ApplicantDTO>> GetApplicants(CancellationToken cancellationToken)
    {
        var entities = await _dbContext.Applicants
            .Include(x => x.Roles)
            .ToListAsync(cancellationToken);

        var result = new List<ApplicantDTO>(); 

        foreach (var item in entities) 
        {
            var roleDtos = item.Roles.Select(x => x.ToRoleDTO()).ToList();

            result.Add(item.ToApplicantDTO(roleDtos));
        }

        return result;
    }

    public async Task<ApplicantDTO> GetApplicant(string id, CancellationToken cancellationToken)
    {
        var entity = await _dbContext.Applicants
            .Include(x => x.Roles)
            .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

        if (entity == null)
        {
            throw new Exception("Entity does not exist");
        }

        var roleDtos = entity.Roles.Select(x => x.ToRoleDTO()).ToList();

        return entity.ToApplicantDTO(roleDtos);
    }

    public async Task CreateApplicant(CreateUpdateApplicantDTO input, CancellationToken cancellationToken)
    {
        var roles = new List<Role>();

        foreach (var item in input.RoleIds)
        {
            var role = await _dbContext.Roles
                .FirstOrDefaultAsync(x => x.Id == item, cancellationToken);

            if (role == null)
            {
                throw new Exception("Role does not exist");
            }

            roles.Add(role);
        }

        var entity = input.ToApplicantEntity(roles);

        await _dbContext.Applicants.AddAsync(entity, cancellationToken);
        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    public async Task UpdateApplicant(string id, CreateUpdateApplicantDTO input, CancellationToken cancellationToken)
    {
        var entity = await _dbContext.Applicants
            .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

        if (entity == null)
        {
            throw new Exception("Entity does not exist");
        }

        var roles = new List<Role>();

        foreach (var item in input.RoleIds)
        {
            var role = await _dbContext.Roles
                .FirstOrDefaultAsync(x => x.Id == item, cancellationToken);

            if (role == null)
            {
                throw new Exception("Role does not exist");
            }

            roles.Add(role);
        }

        entity.Name = input.Name;
        entity.PhoneNumber = input.PhoneNumber;
        entity.Email = input.Email;
        entity.Roles = roles;

        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    public async Task DeleteApplicant(string id, CancellationToken cancellationToken)
    {
        var entity = await _dbContext.Applicants
            .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

        if (entity == null)
        {
            throw new Exception("Entity does not exist");
        }

        _dbContext.Applicants.Remove(entity);
        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}