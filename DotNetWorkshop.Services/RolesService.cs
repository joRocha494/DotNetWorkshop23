﻿using DotNetWorkshop.Database;
using DotNetWorkshop.Database.Entities;
using DotNetWorkshop.Models.Roles;
using Microsoft.EntityFrameworkCore;

namespace DotNetWorkshop.Services;

public interface IRolesService
{
    Task<List<RoleDTO>> GetRoles(CancellationToken cancellationToken);
    Task<RoleDTO> GetRole(string id, CancellationToken cancellationToken);
    Task CreateRole(CreateUpdateRoleDTO input, CancellationToken cancellationToken);
    Task UpdateRole(string id, CreateUpdateRoleDTO input, CancellationToken cancellationToken);
    Task DeleteRole(string id, CancellationToken cancellationToken);
}

public class RolesService : IRolesService
{
    private readonly DatabaseContext _dbContext;

    public RolesService(DatabaseContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<List<RoleDTO>> GetRoles(CancellationToken cancellationToken)
    {
        var entities = await _dbContext.Roles
            .Include(x => x.Applicants)
            .ToListAsync(cancellationToken);

        return entities.Select(x => x.ToRoleDTO()).ToList();
    }

    public async Task<RoleDTO> GetRole(string id, CancellationToken cancellationToken)
    {
        var entity = await _dbContext.Roles
            .Include(x => x.Applicants)
            .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

        if (entity == null)
        {
            throw new Exception("Entity does not exist");
        }

        return entity.ToRoleDTO();
    }

    public async Task CreateRole(CreateUpdateRoleDTO input, CancellationToken cancellationToken)
    {
        var entity = input.ToRoleEntity();

        await _dbContext.Roles.AddAsync(entity, cancellationToken);
        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    public async Task UpdateRole(string id, CreateUpdateRoleDTO input, CancellationToken cancellationToken)
    {
        var entity = await _dbContext.Roles
            .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

        if (entity == null)
        {
            throw new Exception("Entity does not exist");
        }

        entity.Name = input.Name;

        await _dbContext.SaveChangesAsync(cancellationToken);
    }

    public async Task DeleteRole(string id, CancellationToken cancellationToken)
    {
        var entity = await _dbContext.Roles
            .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

        if (entity == null)
        {
            throw new Exception("Entity does not exist");
        }

        _dbContext.Roles.Remove(entity);
        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}